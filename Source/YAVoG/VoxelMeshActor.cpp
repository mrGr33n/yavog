// Fill out your copyright notice in the Description page of Project Settings.

#include "YAVoG.h"
#include "GenerateFace.h"
#include "VoxelMeshActor.h"

// Sets default values
AVoxelMeshActor::AVoxelMeshActor() : Node(nullptr)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = Mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("Mesh"));
}

// Called every frame
void AVoxelMeshActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Node != nullptr && !Node->IsValidLowLevel())
		Node = nullptr;

	if (Node != nullptr && Node->IsDirty())
	{
		Mesh->ClearAllMeshSections();
		TArray<FVector> Vertices;
		TArray<int32> Triangles;
		TArray<FVector> Normals;
		TArray<FVector2D> UVs;
		TArray<FColor> Colors;
		TArray<FProcMeshTangent> Tangents;

		if (Node->IsLeaf())
			GenerateLeaf(Cast<UVoxelLeaf>(Node), Vertices, Triangles, Normals, UVs, Colors, Tangents);
		else
			GenerateBranch(Cast<UVoxelBranch>(Node), Vertices, Triangles, Normals, UVs, Colors, Tangents);

		if (Vertices.Num() == 0)
			Destroy();
		else
			Mesh->CreateMeshSection(0, Vertices, Triangles, Normals, UVs, Colors, Tangents, true);
	}
}

void AVoxelMeshActor::GenerateBranch(UVoxelBranch* Branch, TArray<FVector>& Vertices, TArray<int32>& Triangles,
	TArray<FVector>& Normals, TArray<FVector2D>& UVs, TArray<FColor>& Colors,
	TArray<FProcMeshTangent>& Tangents)
{
	check(Branch != nullptr);
	for (int32 i = 0; i < 8; i++)
	{
		if (Branch->Children[i] != nullptr && Branch->Children[i]->IsDirty())
		{
			if (Branch->Children[i]->IsLeaf())
				GenerateLeaf(Cast<UVoxelLeaf>(Branch->Children[i]), Vertices, Triangles, Normals, UVs, Colors, Tangents);
			else
				GenerateBranch(Cast<UVoxelBranch>(Branch->Children[i]), Vertices, Triangles, Normals, UVs, Colors, Tangents);
		}
	}
}

void AVoxelMeshActor::GenerateLeaf(UVoxelLeaf* Leaf, TArray<FVector>& Vertices, TArray<int32>& Triangles,
	TArray<FVector>& Normals, TArray<FVector2D>& UVs, TArray<FColor>& Colors,
	TArray<FProcMeshTangent>& Tangents)
{
	check(Leaf != nullptr);
	const FInt3 MinVoxelCoordinate = Leaf->Origin - FInt3::Scalar(Leaf->HalfSideLength);

	for (int32 x = 0; x < LEAF_DIM; x++) for (int32 y = 0; y < LEAF_DIM; y++) for (int32 z = 0; z < LEAF_DIM; z++)
	{
		if (Leaf->Data[IndexAt(x, y, z)] > 0)
		{
			FInt3 Coordinate(x, y, z);

			if (z < LEAF_DIM - 1 && Leaf->Data[IndexAt(x, y, z + 1)] == 0)
				GenerateFaceUp(Coordinate, Vertices, Triangles, Normals, UVs, Colors, Tangents);
			if (z == LEAF_DIM - 1)
				GenerateFaceUp(Coordinate, Vertices, Triangles, Normals, UVs, Colors, Tangents);

			if (z > 0 && Leaf->Data[IndexAt(x, y, z - 1)] == 0)
				GenerateFaceDown(Coordinate, Vertices, Triangles, Normals, UVs, Colors, Tangents);
			if (z == 0)
				GenerateFaceDown(Coordinate, Vertices, Triangles, Normals, UVs, Colors, Tangents);

			if (y < LEAF_DIM - 1 && Leaf->Data[IndexAt(x, y + 1, z)] == 0)
				GenerateFaceSouth(Coordinate, Vertices, Triangles, Normals, UVs, Colors, Tangents);
			if (y == LEAF_DIM - 1)
				GenerateFaceSouth(Coordinate, Vertices, Triangles, Normals, UVs, Colors, Tangents);

			if (y > 0 && Leaf->Data[IndexAt(x, y - 1, z)] == 0)
				GenerateFaceNorth(Coordinate, Vertices, Triangles, Normals, UVs, Colors, Tangents);
			if (y == 0)
				GenerateFaceNorth(Coordinate, Vertices, Triangles, Normals, UVs, Colors, Tangents);

			if (x < LEAF_DIM - 1 && Leaf->Data[IndexAt(x + 1, y, z)] == 0)
				GenerateFaceEast(Coordinate, Vertices, Triangles, Normals, UVs, Colors, Tangents);
			if (x == LEAF_DIM - 1)
				GenerateFaceEast(Coordinate, Vertices, Triangles, Normals, UVs, Colors, Tangents);

			if (x > 0 && Leaf->Data[IndexAt(x - 1, y, z)] == 0)
				GenerateFaceWest(Coordinate, Vertices, Triangles, Normals, UVs, Colors, Tangents);
			if (x == 0)
				GenerateFaceWest(Coordinate, Vertices, Triangles, Normals, UVs, Colors, Tangents);
		}
	}

	Leaf->Dirty = false;
}
