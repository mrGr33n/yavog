// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "IntMath.h"
#include "VoxelNode.generated.h"

UCLASS(BlueprintType, Abstract)
class YAVOG_API UVoxelNode : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category = "Voxel Node")
	bool IsInitialized = false;

	UPROPERTY(BlueprintReadOnly, Category = "Voxel Node")
	int32 HalfSideLength = 0;

	UPROPERTY(BlueprintReadOnly, Category = "Voxel Node")
	FInt3 Origin = FInt3(0, 0, 0);

	UPROPERTY(BlueprintReadOnly, Category = "Voxel Node")
	UVoxelNode* Parent = nullptr;

	UVoxelNode();

	virtual ~UVoxelNode();

	UFUNCTION(BlueprintCallable, Category = "Voxel Node")
	virtual void Initialize(int32 NewHalfSideLength, FInt3 const& NewOrigin);

	UFUNCTION(BlueprintCallable, Category = "Voxel Node")
	virtual void Generate();

	UFUNCTION(BlueprintCallable, Category = "Voxel Node")
	virtual bool IsLeaf() const
	{
		return false;
	}

	UFUNCTION(BlueprintCallable, Category = "Voxel Node")
	virtual bool IsEmpty() const
	{
		return true;
	}

	UFUNCTION(BlueprintCallable, Category = "Voxel Node")
	virtual bool IsDirty() const
	{
		return false;
	}

	UFUNCTION(BlueprintCallable, Category = "Voxel Node")
	virtual void SetVoxelAt(FInt3 const& Position, int32 Value);

	UFUNCTION(BlueprintCallable, Category = "Voxel Node")
	virtual int32 GetVoxelAt(FInt3 const& Position) const;
};
