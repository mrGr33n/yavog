// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VoxelNode.h"
#include "VoxelBranch.generated.h"

UCLASS(BlueprintType)
class YAVOG_API UVoxelBranch : public UVoxelNode
{
	GENERATED_BODY()

public:
	UPROPERTY()
	UVoxelNode* Children[8];

	UVoxelBranch();

	virtual ~UVoxelBranch();

	virtual void Initialize(int32 NewHalfSideLength, FInt3 const& NewOrigin) override;

	virtual void Generate() override;

	virtual bool IsLeaf() const override
	{
		return false;
	}

	virtual bool IsEmpty() const override;

	virtual bool IsDirty() const override;

	virtual void SetVoxelAt(FInt3 const& Position, int32 Value) override;

	virtual int32 GetVoxelAt(FInt3 const& Position) const override;

protected:
	static int32 GetOctantContainingVoxel(FInt3 const& Point, FInt3 const& Origin);
};
