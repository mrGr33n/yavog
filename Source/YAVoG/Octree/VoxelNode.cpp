// Fill out your copyright notice in the Description page of Project Settings.

#include "../YAVoG.h"
#include "VoxelNode.h"

UVoxelNode::UVoxelNode()
{ }

UVoxelNode::~UVoxelNode()
{ }

void UVoxelNode::Initialize(int32 NewHalfSideLength, FInt3 const& NewOrigin)
{
	check(!IsInitialized);
	check(FMath::IsPowerOfTwo(NewHalfSideLength));
	HalfSideLength = NewHalfSideLength;
	Origin = NewOrigin;
	IsInitialized = true;
}

void UVoxelNode::Generate()
{
	unimplemented();
}

void UVoxelNode::SetVoxelAt(FInt3 const& Position, int32 Value)
{
	unimplemented();
}

int32 UVoxelNode::GetVoxelAt(FInt3 const& Position) const
{
	unimplemented();
	return 0;
}
