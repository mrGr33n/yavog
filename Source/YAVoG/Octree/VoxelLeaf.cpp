// Fill out your copyright notice in the Description page of Project Settings.

#include "YAVoG.h"
#include "VoxelLeaf.h"

UVoxelGenerator* UVoxelLeaf::Generator = nullptr;

UVoxelLeaf::UVoxelLeaf() : Empty(true), Dirty(false)
{ }

UVoxelLeaf::~UVoxelLeaf()
{ }

void UVoxelLeaf::Initialize(int32 NewHalfSideLength, FInt3 const& NewOrigin)
{
	check(NewHalfSideLength == LEAF_DIM / 2);
	Super::Initialize(NewHalfSideLength, NewOrigin);

	for (int32 i = 0; i < LEAF_DIM * LEAF_DIM * LEAF_DIM; i++)
		Data[i] = 0;
}

void UVoxelLeaf::Generate()
{
	check(IsInitialized);
	if (!Generator->IsValidLowLevel()) Generator = nullptr;
	if (Generator == nullptr)
	{
		Generator = NewObject<UVoxelGenerator>();
		Generator->Initialize(1);
	}

	const FInt3 MinVoxelCoordinate = Origin - FInt3::Scalar(HalfSideLength);
	FGraphEventArray GenerationEvents;
	for (int32 x = 0; x < LEAF_DIM; x++) for (int32 y = 0; y < LEAF_DIM; y++)
	{
		GenerationEvents.Add(FFunctionGraphTask::CreateAndDispatchWhenReady([this, x, y, MinVoxelCoordinate]()
		{
			int32 Height = Generator->GetHeightAt(MinVoxelCoordinate.X + x, MinVoxelCoordinate.Y + y);
			for (int32 z = 0; z < LEAF_DIM; z++)
			{
				FInt3 Pos = MinVoxelCoordinate + FInt3(x, y, z);
				if (Pos.Z <= Height)
					Data[IndexAt(x, y, z)] = 1;
				if (Data[IndexAt(x, y, z)] > 0)
					Empty = false;
			}
		}, TStatId(), nullptr));
	}
	FTaskGraphInterface::Get().WaitUntilTasksComplete(GenerationEvents, ENamedThreads::GameThread);
	Dirty = !Empty;
}

void UVoxelLeaf::SetVoxelAt(FInt3 const& Position, int32 Value)
{
	check(IsInitialized);
	check(Value >= 0 && Value <= MAX_uint16);

	FInt3 Index = Position - (Origin - FInt3::Scalar(HalfSideLength));
	Data[IndexAt(Index.X, Index.Y, Index.Z)] = (uint16)Value;

	if (Value > 0) Empty = false;
	Dirty = true;
}

int32 UVoxelLeaf::GetVoxelAt(FInt3 const& Position) const
{
	check(IsInitialized);

	FInt3 Index = Position - (Origin - FInt3::Scalar(HalfSideLength));
	check(Index.X < LEAF_DIM && Index.X >= 0);
	check(Index.Y < LEAF_DIM && Index.Y >= 0);
	check(Index.Z < LEAF_DIM && Index.Z >= 0);
	return Data[IndexAt(Index.X, Index.Y, Index.Z)];
}
