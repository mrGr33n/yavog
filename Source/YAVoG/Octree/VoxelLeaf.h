// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VoxelNode.h"
#include "VoxelGenerator.h"
#include "VoxelLeaf.generated.h"

#define LEAF_DIM 16

FORCEINLINE static int32 IndexAt(FInt3 const& Index)
{
	return Index.Z + (Index.Y * LEAF_DIM + Index.X) * LEAF_DIM;
}

FORCEINLINE static int32 IndexAt(int32 X, int32 Y, int32 Z)
{
	return IndexAt(FInt3(X, Y, Z));
}

UCLASS(BlueprintType)
class YAVOG_API UVoxelLeaf : public UVoxelNode
{
	GENERATED_BODY()

	static UVoxelGenerator* Generator;

public:
	UPROPERTY()
	bool Empty;

	UPROPERTY()
	bool Dirty;

	UPROPERTY()
	uint16 Data[16 * 16 * 16];

	UVoxelLeaf();

	virtual ~UVoxelLeaf();

	virtual void Initialize(int32 NewHalfSideLength, FInt3 const& NewOrigin) override;

	virtual void Generate() override;

	virtual bool IsLeaf() const override
	{
		return true;
	}

	virtual bool IsEmpty() const override
	{
		return Empty;
	}

	virtual bool IsDirty() const override
	{
		return Dirty;
	}

	virtual void SetVoxelAt(FInt3 const& Position, int32 Value) override;

	virtual int32 GetVoxelAt(FInt3 const& Position) const override;
};
