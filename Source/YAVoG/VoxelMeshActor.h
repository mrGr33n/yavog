// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ProceduralMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Octree/VoxelBranch.h"
#include "Octree/VoxelLeaf.h"
#include "VoxelMeshActor.generated.h"

UCLASS()
class YAVOG_API AVoxelMeshActor : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadOnly, Category = "Voxel Mesh")
	UVoxelNode* Node;

	UPROPERTY(BlueprintReadOnly, Category = "Voxel Mesh")
	UProceduralMeshComponent* Mesh;

	// Sets default values for this actor's properties
	AVoxelMeshActor();
	
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	virtual void GenerateBranch(UVoxelBranch* Leaf, TArray<FVector>& Vertices, TArray<int32>& Triangles,
		TArray<FVector>& Normals, TArray<FVector2D>& UVs, TArray<FColor>& Colors,
		TArray<FProcMeshTangent>& Tangents);

	virtual void GenerateLeaf(UVoxelLeaf* Leaf, TArray<FVector>& Vertices, TArray<int32>& Triangles,
		TArray<FVector>& Normals, TArray<FVector2D>& UVs, TArray<FColor>& Colors,
		TArray<FProcMeshTangent>& Tangents);
};
